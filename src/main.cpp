#include "ftxui/component/captured_mouse.hpp"      // for ftxui
#include "ftxui/component/component.hpp"           // for Menu
#include "ftxui/component/component_base.hpp"      // for Menu
#include "ftxui/component/component_options.hpp"   // for MenuOption
#include "ftxui/component/screen_interactive.hpp"  // for ScreenInteractive

#include <fmt/core.h>
#include <ftxui/dom/elements.hpp>
#include <ftxui/dom/table.hpp>

#define MINIAUDIO_IMPLEMENTATION
#include <complex>
#include <filesystem>
#include <miniaudio.h>
#include <vector>

static const uint32_t FPS = 30u;
static const float PI = acosf(-1.f);
static const auto I = std::complex<float>(0.f, 1.f);

static inline float hann_smooth(float input) {
    return 0.5f - 0.5f * cosf(2.f * PI * input);
}

[[maybe_unused]] static inline void fft(std::vector<float> in, std::vector<std::complex<float>> out, size_t stride) {
    size_t n = in.size();

    if (n == 0)
        return;

    if (n == 1) {
        out.at(0) = std::real(in[0]);
        return;
    }

    for (size_t i = 0; i < n / 2; ++i) {
        float t = (float) i / n;
        std::complex<float> a = std::exp(I * -2.f * PI);
        // std::complex<float> b = std
    }
}

void audio_callback(ma_device *device, void *output, const void *input, ma_uint32 len) {}

// will be this way until variable options are implemented
struct Options {
public:
    Options() {}

    int SampleRate = 48000;
    int Channels = 2;
    int BPM = 160;
    const char *Name = "Default Name";
};

// Encoding support: only WAV
// Decoding support: MP3, WAV, FLAC

// TODO(jsfer): check for user audio output device setup, if stereo/mono
// supported
// TODO(jsfer): Resampling

int main() {
    auto options = Options();

    ma_device_config audio_config = ma_device_config_init(ma_device_type_playback);
    audio_config.playback.format = ma_format_f32;
    audio_config.playback.channels = options.Channels;
    audio_config.sampleRate = options.SampleRate;
    audio_config.dataCallback = audio_callback;
    audio_config.pUserData = NULL;

    ma_device audio_device;
    if (ma_device_init(NULL, &audio_config, &audio_device) != MA_SUCCESS) {
        fmt::print(stderr, "Could not init miniaudio device\n");
        return -1;
    }

    ma_device_start(&audio_device);

    ma_engine audio_engine;
    if (ma_engine_init(NULL, &audio_engine)) {
        fmt::print(stderr, "Could not init miniaudio device\n");
        return -1;
    }

    using namespace ftxui;
    auto screen = ScreenInteractive::Fullscreen();

    auto right = Renderer([] { return text("right") | center; });
    int right_size = 0;

    auto grid = Renderer([&] {
            return gridbox({
                    // clang-format off
                    {
                        text("The quick brown fox jumps over the lazy dog."),
                        text("The quick brown fox jumps over the lazy dog."),
                        text("The quick brown fox jumps over the lazy dog."),
                        text("The quick brown fox jumps over the lazy dog."),
                    },
                    {
                        text("The quick brown fox jumps over the lazy dog."),
                        text("The quick brown fox jumps over the lazy dog."),
                        text("The quick brown fox jumps over the lazy dog."),
                        text("The quick brown fox jumps over the lazy dog."),
                    }
                    // clang-format on
            });
    });
    auto container = grid;
    container = ResizableSplitRight(right, container, &right_size);

    auto renderer = Renderer(container, [&] { return container->Render(); });

    auto top = Renderer([&] {
        std::string content = fmt::format("Project: {}    BPM: {}", options.Name, options.BPM);
        return hbox({ text(content) });
    });

    auto bottom = Renderer([] { return hbox({ text("status") }); });
    auto topbar = Renderer([] { return separator(); });
    auto bottombar = Renderer([] { return separator(); });

    auto whole = Container::Vertical({
                         top,
                         topbar,
                         renderer,
                         bottombar,
                         bottom,
                 })
                 | borderHeavy;

    screen.Loop(whole);

    ma_engine_uninit(&audio_engine);
    ma_device_uninit(&audio_device);
}
